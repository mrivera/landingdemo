<?php
use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\Exception;

require 'Exception.php';
require 'PHPMailer.php';
require 'SMTP.php';

$name = $_POST['name'];
$lastname = $_POST['lastname'];
$company = $_POST['company'];
$email = $_POST['email'];

//Create an instance; 
$mail = new PHPMailer(true);
try {
   //Server settings        
    $mail = new PHPMailer();
    $mail->isSMTP();
    $mail->Host = '';
    $mail->SMTPAuth = true;
    $mail->Port = 465;
    $mail->Username = '';
    $mail->Password = '';  

    //Recipients
    $mail->setFrom($email, $name);
    $mail->addAddress('marketing@arkondata.com', 'Arkon Data');     //Add a recipient

    //Content
    $mail->isHTML(true);                                  //Set email format to HTML
    $mail->Subject = 'Arkon Data Form';
    $mail->Body    = '<div style="border:2px solid #3DC0CB; padding: 20px; border-radius:20px;margin:40px;"><div style="text-align:center; color:#009FDC;margin-bottom:10px;">This message has been sent from the Arkon Data contact form</div> <div style="color:#4A4A4A"><strong>Name: </strong>'.$name.'<div><br> <div style="color:#4A4A4A"><strong>Lastname: </strong>'.$lastname.'</div><br> <div style="color:#4A4A4A"><strong>Company: </strong>'.$company.'</div><br><div style="color:#4A4A4A"><strong>Email: </strong>'.$email.'</div></div>';

    $mail->send();
    echo 'Message has been sent';
} catch (Exception $e) {
    echo "Message could not be sent. Mailer Error: {$mail->ErrorInfo}";
}